package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.RawSensorSet
import de.tadris.recognizr.data.SensorSample
import de.tadris.recognizr.data.TimedSensorSample
import org.junit.Assert.*
import org.junit.Test

class SensorSetPackerTest {

    @Test
    fun testSensorPackerUpsampling(){
        val dataset = RawSensorSet(listOf(
            TimedSensorSample(0, SensorSample(0.0)),
            TimedSensorSample(PackedSensorSet.DURATION_MICRO_SECONDS, SensorSample(1.0)),
        ))

        val packed = SensorSetPacker(dataset).pack()

        assertEquals(packed.samples.size, PackedSensorSet.SAMPLE_COUNT)
        assertEquals(packed.samples.first().acceleration, 0.0, 0.01)
        assertEquals(packed.samples.last().acceleration, 1.0, 0.01)
    }

    @Test
    fun testSensorPackerResamplingComplex(){
        val offset = 0L
        val dataset = RawSensorSet(listOf(
            TimedSensorSample(offset + 0, SensorSample(0.01695663749249557)),
            TimedSensorSample(offset + 16884, SensorSample(0.008151533426770594)),
            TimedSensorSample(offset + 33766, SensorSample(0.008431861455996614)),
            TimedSensorSample(offset + 50650, SensorSample(0.010926117161902567)),
            TimedSensorSample(offset + 67532, SensorSample(0.01284959609836565)),
            TimedSensorSample(offset + 84416, SensorSample(0.01438376710556636)),
            TimedSensorSample(offset + 101298, SensorSample(0.01611136862292031)),
            TimedSensorSample(offset + 118182, SensorSample(0.012242745215161444)),
            TimedSensorSample(offset + 135065, SensorSample(0.007687039274620945)),
            TimedSensorSample(offset + 151942, SensorSample(0.016146953838189416)),
            TimedSensorSample(offset + 168830, SensorSample(0.008761446755759959)),
            TimedSensorSample(offset + 185714, SensorSample(0.01497588031390662)),
            TimedSensorSample(offset + 202599, SensorSample(0.008193551539832304)),
            TimedSensorSample(offset + 219479, SensorSample(0.010996510569619304)),
            TimedSensorSample(offset + 236355, SensorSample(0.007671605288041055)),
            TimedSensorSample(offset + 253247, SensorSample(0.007669268911260535)),
            TimedSensorSample(offset + 270129, SensorSample(0.01018410267621428)),
            TimedSensorSample(offset + 287013, SensorSample(0.008162429409811921)),
            TimedSensorSample(offset + 303895, SensorSample(0.010799222945382693)),
            TimedSensorSample(offset + 320776, SensorSample(0.01000994013593231)),
            TimedSensorSample(offset + 337654, SensorSample(0.007564320053028734)),
            TimedSensorSample(offset + 354545, SensorSample(0.012712161482347967)),
            TimedSensorSample(offset + 371427, SensorSample(0.014579856183705246)),
            TimedSensorSample(offset + 388311, SensorSample(0.013367173946369094)),
            TimedSensorSample(offset + 405194, SensorSample(0.01886031058797156)),
            TimedSensorSample(offset + 422078, SensorSample(0.01884692921694588)),
            TimedSensorSample(offset + 438960, SensorSample(0.011391963826366305)),
            TimedSensorSample(offset + 455844, SensorSample(0.026084415601822307)),
            TimedSensorSample(offset + 472726, SensorSample(0.012939488919856246)),
            TimedSensorSample(offset + 489610, SensorSample(0.011146759012291474)),
            TimedSensorSample(offset + 506494, SensorSample(0.012058211514411901)),
            TimedSensorSample(offset + 523364, SensorSample(0.012358956712910114)),
            TimedSensorSample(offset + 540259, SensorSample(0.013626234246020955)),
            TimedSensorSample(offset + 557142, SensorSample(0.010890772759516297)),
            TimedSensorSample(offset + 574025, SensorSample(0.014781295075217962)),
            TimedSensorSample(offset + 590908, SensorSample(0.013147540484496123)),
            TimedSensorSample(offset + 607789, SensorSample(0.013259141196779704)),
            TimedSensorSample(offset + 624677, SensorSample(0.023011012491223167)),
            TimedSensorSample(offset + 641557, SensorSample(0.013386758033288412)),
            TimedSensorSample(offset + 658441, SensorSample(0.008591348514543224)),
            TimedSensorSample(offset + 675323, SensorSample(0.015877527850302397)),
            TimedSensorSample(offset + 692207, SensorSample(0.013728933377623384)),
            TimedSensorSample(offset + 709089, SensorSample(0.028511777136423756)),
            TimedSensorSample(offset + 725973, SensorSample(0.014455625411299494)),
            TimedSensorSample(offset + 742856, SensorSample(0.013268437147505406)),
            TimedSensorSample(offset + 759740, SensorSample(0.014850640526481423)),
            TimedSensorSample(offset + 776622, SensorSample(0.01205557201358678)),
            TimedSensorSample(offset + 793505, SensorSample(0.015151861623888598)),
            TimedSensorSample(offset + 810389, SensorSample(0.02020451664809397)),
            TimedSensorSample(offset + 827272, SensorSample(0.00781348475980993)),
            TimedSensorSample(offset + 844144, SensorSample(0.019364154611237576)),
            TimedSensorSample(offset + 861038, SensorSample(0.016739279700857753)),
            TimedSensorSample(offset + 877920, SensorSample(0.01592291809827752)),
            TimedSensorSample(offset + 894804, SensorSample(0.010718072272752172)),
            TimedSensorSample(offset + 911676, SensorSample(0.010719516638755588)),
            TimedSensorSample(offset + 928560, SensorSample(0.010716603624664794)),
            TimedSensorSample(offset + 945453, SensorSample(0.01672315819189831)),
            TimedSensorSample(offset + 962337, SensorSample(0.009263783716068565)),
            TimedSensorSample(offset + 979219, SensorSample(0.015960178971232645)),
            TimedSensorSample(offset + 996103, SensorSample(0.007783272398300429)),
            TimedSensorSample(offset + 1012985, SensorSample(0.013148431278791367)),
            TimedSensorSample(offset + 1029869, SensorSample(0.024091409961209405)),
            TimedSensorSample(offset + 1046751, SensorSample(0.012120355611649739)),
            TimedSensorSample(offset + 1063635, SensorSample(0.04440200523802522)),
            TimedSensorSample(offset + 1080517, SensorSample(0.7757846137338077)),
            TimedSensorSample(offset + 1097401, SensorSample(1.6038571392279717)),
            TimedSensorSample(offset + 1114284, SensorSample(2.0299376584058324)),
            TimedSensorSample(offset + 1198700, SensorSample(4.892880216779489)),
            TimedSensorSample(offset + 1215582, SensorSample(1.6397900596006632)),
            TimedSensorSample(offset + 1232466, SensorSample(3.568324540182895)),
            TimedSensorSample(offset + 1249346, SensorSample(6.882471496339004)),
            TimedSensorSample(offset + 1266234, SensorSample(6.521275146293491)),
            TimedSensorSample(offset + 1283115, SensorSample(2.6003819230718066)),
            TimedSensorSample(offset + 1299999, SensorSample(1.6717969999371034)),
            TimedSensorSample(offset + 1316880, SensorSample(2.0164832010321945)),
            TimedSensorSample(offset + 1333764, SensorSample(2.8720751545229817)),
            TimedSensorSample(offset + 1350647, SensorSample(1.8574276275766428)),
            TimedSensorSample(offset + 1367531, SensorSample(1.1419058986335955)),
            TimedSensorSample(offset + 1384413, SensorSample(1.5036506462283874)),
            TimedSensorSample(offset + 1401297, SensorSample(1.0222598511155834)),
            TimedSensorSample(offset + 1418179, SensorSample(2.037938057184923)),
            TimedSensorSample(offset + 1435060, SensorSample(3.777665659212933)),
            TimedSensorSample(offset + 1451948, SensorSample(3.037030753721712)),
            TimedSensorSample(offset + 1468829, SensorSample(1.355618570658253)),
            TimedSensorSample(offset + 1485712, SensorSample(0.148516848485369)),
            TimedSensorSample(offset + 1502595, SensorSample(0.9617554790565066)),
            TimedSensorSample(offset + 1519474, SensorSample(0.8701605774756911)),
            TimedSensorSample(offset + 1536359, SensorSample(1.518628013983411)),
            TimedSensorSample(offset + 1553247, SensorSample(2.213929389008802)),
            TimedSensorSample(offset + 1570134, SensorSample(3.7593396266002856)),
            TimedSensorSample(offset + 1587010, SensorSample(4.228948592271356)),
            TimedSensorSample(offset + 1603894, SensorSample(3.3175885907052605)),
            TimedSensorSample(offset + 1620776, SensorSample(3.7352991570044893)),
            TimedSensorSample(offset + 1637660, SensorSample(2.229465736425287)),
            TimedSensorSample(offset + 1654542, SensorSample(1.7642328397843048)),
            TimedSensorSample(offset + 1671413, SensorSample(3.3571330913552377)),
            TimedSensorSample(offset + 1688308, SensorSample(2.1118088447953527)),
            TimedSensorSample(offset + 1705195, SensorSample(1.2488848194015862)),
            TimedSensorSample(offset + 1722072, SensorSample(0.4998569434314319)),
            TimedSensorSample(offset + 1738962, SensorSample(0.38161575706350526)),
            TimedSensorSample(offset + 1755831, SensorSample(0.3538821384052196)),
            TimedSensorSample(offset + 1772725, SensorSample(0.6551592424052826)),
            TimedSensorSample(offset + 1789607, SensorSample(0.8935224409544268)),
            TimedSensorSample(offset + 1806481, SensorSample(1.9725668121377693)),
            TimedSensorSample(offset + 1823373, SensorSample(3.1151171438823795)),
            TimedSensorSample(offset + 1840257, SensorSample(4.635898842077845)),
            TimedSensorSample(offset + 1857142, SensorSample(4.644084462024228)),
            TimedSensorSample(offset + 1874023, SensorSample(4.315856908347368)),
            TimedSensorSample(offset + 1890906, SensorSample(4.609543549911126)),
            TimedSensorSample(offset + 1907789, SensorSample(4.537225725052238)),
            TimedSensorSample(offset + 1924673, SensorSample(4.45499646830532)),
            TimedSensorSample(offset + 1941543, SensorSample(3.742645138888266)),
            TimedSensorSample(offset + 1958428, SensorSample(3.3789296507609947)),
            TimedSensorSample(offset + 1975325, SensorSample(4.024024570134834)),
            TimedSensorSample(offset + 1992201, SensorSample(3.508553590958317)),
            TimedSensorSample(offset + 2009091, SensorSample(2.547445617609426)),
            TimedSensorSample(offset + 2025960, SensorSample(2.3594235112347723)),
            TimedSensorSample(offset + 2042854, SensorSample(2.3074249575208787)),
            TimedSensorSample(offset + 2059737, SensorSample(2.1732523960445826)),
            TimedSensorSample(offset + 2076616, SensorSample(2.341561650478788)),
            TimedSensorSample(offset + 2093503, SensorSample(2.403635764795217)),
            TimedSensorSample(offset + 2110387, SensorSample(2.511933018218919)),
            TimedSensorSample(offset + 2127269, SensorSample(2.8252095100599983)),
            TimedSensorSample(offset + 2144153, SensorSample(3.8938449389641465)),
            TimedSensorSample(offset + 2161035, SensorSample(4.8253468259744405)),
            TimedSensorSample(offset + 2177919, SensorSample(5.80215997551783)),
            TimedSensorSample(offset + 2194790, SensorSample(5.950982379619862)),
            TimedSensorSample(offset + 2211682, SensorSample(5.403949705429305)),
            TimedSensorSample(offset + 2228570, SensorSample(4.5972381454934705)),
            TimedSensorSample(offset + 2245451, SensorSample(4.286676070585543)),
            TimedSensorSample(offset + 2262335, SensorSample(4.48644206862503)),
            TimedSensorSample(offset + 2279213, SensorSample(3.413908584582696)),
            TimedSensorSample(offset + 2296102, SensorSample(1.1411313660102294)),
            TimedSensorSample(offset + 2312993, SensorSample(0.6796670680966994)),
            TimedSensorSample(offset + 2329866, SensorSample(0.802336665181084)),
            TimedSensorSample(offset + 2346750, SensorSample(1.3316826134473059)),
            TimedSensorSample(offset + 2363633, SensorSample(1.3947045520197243)),
            TimedSensorSample(offset + 20490000, SensorSample(1.592372015149704)),
        ))

        val packed = SensorSetPacker(dataset).pack()

        assertEquals(0.010716, packed.samples[46].acceleration, 0.001)

    }

}