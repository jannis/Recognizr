package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.SensorSample
import org.junit.Assert.*
import org.junit.Test
import kotlin.math.sin

class FastFourierTransformationTest {

    @Test
    fun testTransformation(){
        // Create set containing a simple sine wave
        val set = PackedSensorSet(buildList {
            repeat(PackedSensorSet.SAMPLE_COUNT){
                add(SensorSample(sin(it / 10.0)))
            }
        })

        val result = FastFourierTransformation.transform(set)
        println(result)

        val max = result.magnitudes.max()
        val indexOfMax = result.magnitudes.indexOfFirst { it == max }

        assertEquals(16, indexOfMax) // Check the expected maximum
    }

}