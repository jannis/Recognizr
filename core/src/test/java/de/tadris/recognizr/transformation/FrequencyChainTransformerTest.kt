package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.SensorFrequenciesSet
import de.tadris.recognizr.data.SensorSample
import de.tadris.recognizr.recording.TransformedSetReceiver
import org.junit.Assert.*
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class FrequencyChainTransformerTest {

    @Test
    fun testReceiveSet(){
        // Prepare query and result
        val querySet = PackedSensorSet(buildList {
            repeat(PackedSensorSet.SAMPLE_COUNT){
                add(SensorSample(1.0))
            }
        })
        val resultSet = SensorFrequenciesSet(doubleArrayOf(1.0, 2.0, 3.0))

        // Setup mock transformation and receiver
        val transformation: FrequencyTransformation = mock {
            on { transform(any()) }.thenAnswer { resultSet }
        }
        val receiver: TransformedSetReceiver = mock()

        // Invoke onReceiveSet
        val transformer = FrequencyChainTransformer(transformation, receiver)
        transformer.onReceiveSet(querySet)

        // Verify that the result has been given to the receiver
        verify(receiver).onReceiveSet(resultSet)
    }

}