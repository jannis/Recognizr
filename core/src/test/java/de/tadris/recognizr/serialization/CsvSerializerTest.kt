package de.tadris.recognizr.serialization

import de.tadris.recognizr.data.SensorFrequenciesSet
import org.junit.Test
import org.junit.Assert.*
import java.io.ByteArrayOutputStream

class CsvSerializerTest {

    @Test
    fun testBuildSerializer(){
        CsvSerializerFactory.buildSerializer(ByteArrayOutputStream())
    }

    @Test
    fun testSerializeFrequencySets(){
        val outputSink = ByteArrayOutputStream()
        val serializer = CsvFrequenciesSerializer(outputSink)

        val testSet1 = SensorFrequenciesSet(doubleArrayOf(20.0, 50.0, 0.0, 0.123))
        val testSet2 = SensorFrequenciesSet(doubleArrayOf(1.0, 2.0, 3.0))
        serializer.serialize(testSet1)
        serializer.serialize(testSet2)

        assertEquals("20.0,50.0,0.0,0.123\n1.0,2.0,3.0\n", outputSink.toByteArray().decodeToString())
    }

}