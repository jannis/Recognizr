package de.tadris.recognizr.serialization

import de.tadris.recognizr.data.SensorFrequenciesSet
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import java.io.File
import java.util.*

class FileSerializerTest {

    private lateinit var file: File

    @Before
    fun initFile(){
        file = File(UUID.randomUUID().toString() + ".txt")
    }

    @After
    fun cleanupFile(){
        file.delete()
    }

    @Test
    fun testSerialization(){
        val testString = "hallo\n"
        val testSet = SensorFrequenciesSet(doubleArrayOf(1.0, 2.0))
        val fileSerializer = FileSerializer(file){ stream ->
            mock {
                on { serialize(testSet) }.then {
                    val writer = stream.bufferedWriter()
                    writer.write(testString)
                    writer.flush()
                }
            }
        }
        fileSerializer.onReceiveSet(testSet)

        assertTrue(file.exists())
        assertEquals(testString, file.readText())
    }

}