package de.tadris.recognizr.classifier

import org.junit.Test
import org.junit.Assert.*

class ActivityTypeTest {

    @Test
    fun testFindById(){
        assertEquals(ActivityType.WALKING, ActivityType.findById("walking"))
        assertEquals(ActivityType.UNKNOWN, ActivityType.findById("fhsdafsdf"))
    }

}