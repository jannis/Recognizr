package de.tadris.recognizr.classifier

import de.tadris.recognizr.data.SensorFrequenciesSet
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class ChainedClassifierTest {

    @Test
    fun testChain(){
        val testSet = SensorFrequenciesSet(doubleArrayOf(5.0))

        val classifier = mock<ActivityClassifier> {
            on { classify(any()) } doReturn ActivityType.WALKING
        }
        val listener = mock<ActivityClassificationListener>()

        val chainedClassifier = ChainedClassifier(classifier, listener)
        chainedClassifier.onReceiveSet(testSet)

        verify(classifier).classify(testSet)
        verify(listener).onClassified(ActivityType.WALKING)
    }

}