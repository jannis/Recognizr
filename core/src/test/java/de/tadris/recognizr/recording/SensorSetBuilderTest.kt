package de.tadris.recognizr.recording

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.SensorSample
import de.tadris.recognizr.data.TimedSensorSample
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

class SensorSetBuilderTest {

    @Test(expected = IllegalArgumentException::class)
    fun testTimeDescend(){
        val receiver = mock<PackedSensorSetReceiver>()
        val builder = SensorSetBuilder(receiver)

        builder.receive(TimedSensorSample(500, SensorSample(1.5)))
        builder.receive(TimedSensorSample(2000, SensorSample(1.7)))
        builder.receive(TimedSensorSample(1500, SensorSample(1.8))) // time goes backwards
    }

    @Test
    fun testEmitPackedSet(){
        val receiver = mock<PackedSensorSetReceiver>()
        val builder = SensorSetBuilder(receiver)

        builder.receive(TimedSensorSample(2000, SensorSample(1.5)))
        builder.receive(TimedSensorSample(PackedSensorSet.DURATION_MICRO_SECONDS / 2, SensorSample(20.0)))
        verify(receiver, times(0)).onReceiveSet(any())
        builder.receive(TimedSensorSample(PackedSensorSet.DURATION_MICRO_SECONDS + 5000, SensorSample(5.0)))
        verify(receiver, times(1)).onReceiveSet(any()) // successfully emitted a PackedSensorSet
    }

}