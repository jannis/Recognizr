package de.tadris.recognizr.recording

import de.tadris.recognizr.data.SensorFrequenciesSet
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

class MultiTransformedSetReceiverTest {

    @Test
    fun testReceive(){
        val testSet = SensorFrequenciesSet(doubleArrayOf(5.0))

        val receiver1 = mock<TransformedSetReceiver>()
        val receiver2 = mock<TransformedSetReceiver>()

        val multiReceiver = MultiTransformedSetReceiver(listOf(
            receiver1, receiver2
        ))

        multiReceiver.onReceiveSet(testSet)

        verify(receiver1, times(1)).onReceiveSet(testSet)
        verify(receiver2, times(1)).onReceiveSet(testSet)
    }

}