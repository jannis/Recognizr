package de.tadris.recognizr.data

import org.junit.Test
import org.junit.Assert.*

class PackedSensorSetTest {

    @Test(expected = IllegalArgumentException::class)
    fun testSensorSetWrongSize(){
        PackedSensorSet(listOf(
            SensorSample(1.0)
        ))
    }

    @Test
    fun testSensorSetCorrectSize(){
        PackedSensorSet(buildList {
            repeat(PackedSensorSet.SAMPLE_COUNT){
                add(SensorSample(1.0))
            }
        })
    }

}