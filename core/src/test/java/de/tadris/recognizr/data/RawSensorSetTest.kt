package de.tadris.recognizr.data

import org.junit.Test
import org.junit.Assert.*

class RawSensorSetTest {

    @Test
    fun testStartEnd(){
        val set = RawSensorSet(listOf(
            TimedSensorSample(100, SensorSample(0.0)),
            TimedSensorSample(1000, SensorSample(1.0))
        ))
        assertEquals(set.start, 100)
        assertEquals(set.end, 1000)
        assertEquals(set.duration, 900)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testSensorSetEmpty(){
        RawSensorSet(listOf())
    }

    @Test(expected = IllegalArgumentException::class)
    fun testNotEnoughSamples(){
        RawSensorSet(listOf(TimedSensorSample(100, SensorSample(1.0))))
    }

}