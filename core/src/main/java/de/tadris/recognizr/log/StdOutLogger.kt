package de.tadris.recognizr.log

/**
 * A logger implementation using the default [println]
 */
object StdOutLogger : Logger {

    override fun d(tag: String, message: String) {
        println("I[$tag] $message")
    }

    override fun w(tag: String, message: String) {
        println("W[$tag] $message")
    }
}