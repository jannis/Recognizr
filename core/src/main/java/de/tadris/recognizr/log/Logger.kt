package de.tadris.recognizr.log

/**
 * Simple logger interface.
 */
interface Logger {

    companion object {

        /**
         * Currently used logger implementation. Could be replaced by Android logger
         */
        var implementation = StdOutLogger

        /**
         * Logs a debug message using the [implementation]
         *
         * @param tag Logger tag
         * @param message Message
         */
        fun d(tag: String, message: String){
            implementation.d(tag, message)
        }

        /**
         * Logs a warning using the [implementation]
         *
         * @param tag Logger tag
         * @param message Message
         */
        fun w(tag: String, message: String){
            implementation.w(tag, message)
        }

    }

    /**
     * Logs a debug message
     *
     * @param tag Logger tag
     * @param message Message
     */
    fun d(tag: String, message: String)

    /**
     * Logs a warning
     *
     * @param tag Logger tag
     * @param message Message
     */
    fun w(tag: String, message: String)

}