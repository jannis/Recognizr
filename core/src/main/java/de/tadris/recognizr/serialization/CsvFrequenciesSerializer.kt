package de.tadris.recognizr.serialization

import de.tadris.recognizr.data.SensorFrequenciesSet
import java.io.OutputStream
import java.io.IOException

/**
 * Serializes frequency sets into a CSV format (comma separated)
 * The stream will NOT be closed after serialization.
 * @param output Stream to be written to
 */
class CsvFrequenciesSerializer(output: OutputStream): FrequenciesSerializer {

    companion object {
        /**
         * Separator for output CSV files
         */
        const val SEPARATOR = ","
    }

    private val writer = output.bufferedWriter()

    override fun serialize(set: SensorFrequenciesSet) {
        writeSet(set)
        writer.flush()
    }

    private fun writeSet(set: SensorFrequenciesSet){
        set.magnitudes.forEachIndexed { index, magnitude ->
            if(index != 0) writer.write(SEPARATOR)
            writer.write(magnitude.toString())
        }
        writer.newLine()
    }

}