package de.tadris.recognizr.serialization

import de.tadris.recognizr.data.SensorFrequenciesSet
import de.tadris.recognizr.recording.TransformedSetReceiver
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Serializes incoming sensor sets to a file. New sets will be appended to the current file.
 * @property outputFile File to be written to
 */
class FileSerializer(
    private val outputFile: File,
    private val serializerFactory: StreamSerializerFactory,
) : TransformedSetReceiver {

    override fun onReceiveSet(set: SensorFrequenciesSet) {
        appendToFile(set)
    }

    /**
     * Appends a sensor set to the file. Last character of the file should be a newline.
     * @param set Set to be written
     * @throws IOException if file could not be created
     */
    private fun appendToFile(set: SensorFrequenciesSet){
        setupFile()

        val stream = FileOutputStream(outputFile, true)
        serializerFactory.buildSerializer(stream).serialize(set)
        stream.close()
    }

    private fun setupFile(){
        // try to create parent directories
        if(outputFile.parentFile?.exists() == false && outputFile.parentFile?.mkdirs() == false)
            throw IOException("Cannot create parent file ${outputFile.parentFile?.path}")
        if(!outputFile.exists()){
            // create empty file if it doesn't exist
            if(!outputFile.createNewFile()) throw IOException("Cannot create file ${outputFile.path}")
        }
    }

}