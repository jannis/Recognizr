package de.tadris.recognizr.serialization

import java.io.OutputStream

/**
 * Factory for creating [FrequenciesSerializer]s that write to an [OutputStream]
 *
 */
fun interface StreamSerializerFactory {

    /**
     * Build a [FrequenciesSerializer]
     *
     * @param output Stream to be written to
     * @return serializer that writes to the given output stream
     */
    fun buildSerializer(output: OutputStream): FrequenciesSerializer

}