package de.tadris.recognizr.serialization

import de.tadris.recognizr.data.SensorFrequenciesSet

/**
 * Serializes frequency sets.
 */
interface FrequenciesSerializer {

    /**
     * Serializes one frequency set.
     *
     * @param set Set to be serialized
     */
    fun serialize(set: SensorFrequenciesSet)

}