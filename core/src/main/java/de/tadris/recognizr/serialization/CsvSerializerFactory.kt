package de.tadris.recognizr.serialization

import java.io.OutputStream

/**
 * Factory to create a [CsvFrequenciesSerializer]
 */
object CsvSerializerFactory : StreamSerializerFactory {

    override fun buildSerializer(output: OutputStream) = CsvFrequenciesSerializer(output)

}