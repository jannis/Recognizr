package de.tadris.recognizr.data

/**
 * Represents a sensor sample
 *
 * @property acceleration Combined acceleration in m/s²
 */
data class SensorSample(val acceleration: Double){

    override fun toString() = acceleration.toString()

}