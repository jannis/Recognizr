package de.tadris.recognizr.data

/**
 * A raw sensor set with timed sensor samples.
 * @throws IllegalArgumentException if dataset has less then 2 samples
 * @property set Set of timed sensor samples. Must have 2 or more elements
 */
class RawSensorSet(val set: List<TimedSensorSample>){

    init {
        if(set.size < 2) throw IllegalArgumentException("Dataset must at least have 2 samples")
    }

    /**
     * Relative start time of set in microseconds
     */
    val start get() = set.first().timestamp

    /**
     * Relative end time of set in microseconds
     */
    val end get() = set.last().timestamp

    /**
     * Duration of set in microseconds
     */
    val duration get() = end - start

    override fun toString() = set.toString()

}