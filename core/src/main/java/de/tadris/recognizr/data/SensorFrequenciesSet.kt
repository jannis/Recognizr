package de.tadris.recognizr.data

/**
 * Set representing the fourier transformed magnitudes of a sensor set.
 * Typically is 1/2 of [PackedSensorSet.SAMPLE_COUNT] long.
 *
 * @property magnitudes Array of frequency magnitudes
 */
data class SensorFrequenciesSet(val magnitudes: DoubleArray) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SensorFrequenciesSet

        if (!magnitudes.contentEquals(other.magnitudes)) return false

        return true
    }

    override fun hashCode(): Int {
        return magnitudes.contentHashCode()
    }

}