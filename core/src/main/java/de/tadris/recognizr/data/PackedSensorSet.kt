package de.tadris.recognizr.data

/**
 * A sensor set over the time of TIME_SECONDS with every
 * @property samples List of samples
 */
data class PackedSensorSet(val samples: List<SensorSample>){

    init {
        if(samples.size != SAMPLE_COUNT) throw IllegalArgumentException("Sample count must be SAMPLE_COUNT ($SAMPLE_COUNT)")
    }

    override fun toString() = samples.toString()

    companion object {

        /**
         * Sample rate of a sensor set in Hertz
         */
        const val SAMPLE_RATE = 50 // Hz

        /**
         * Count of samples in a sensor set
         */
        const val SAMPLE_COUNT = 1024

        /**
         * Duration of a sensor set in seconds
         */
        const val DURATION_SECONDS = SAMPLE_COUNT.toDouble() / SAMPLE_RATE

        /**
         * Duration of a sensor set in microseconds
         */
        const val DURATION_MICRO_SECONDS = (DURATION_SECONDS * 1000_000L).toLong()

        /**
         * Duration between two samples
         */
        const val SAMPLE_DURATION_MICRO_SECONDS = DURATION_MICRO_SECONDS / SAMPLE_COUNT

    }
}