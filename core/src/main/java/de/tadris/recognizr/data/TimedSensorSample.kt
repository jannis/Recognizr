package de.tadris.recognizr.data

/**
 * Sensor sample with an associated timestamp
 * @property timestamp Relative timestamp in microseconds
 * @property sample Associated sensor sample
 */
data class TimedSensorSample(
    val timestamp: Long,
    val sample: SensorSample,
){

    override fun toString() = "$timestamp: $sample"

}