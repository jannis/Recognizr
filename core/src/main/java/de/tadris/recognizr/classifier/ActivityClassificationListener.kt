package de.tadris.recognizr.classifier

/**
 * An interface listening for new classifications.
 *
 */
fun interface ActivityClassificationListener {

    /**
     * Called when a new activity type was recognized.
     *
     * @param activity Recognized activity type
     */
    fun onClassified(activity: ActivityType)

}