package de.tadris.recognizr.classifier

import de.tadris.recognizr.data.SensorFrequenciesSet
import de.tadris.recognizr.recording.TransformedSetReceiver

/**
 * Receives [SensorFrequenciesSet]s, classifies them using an [ActivityClassifier] and
 * gives them to a [ActivityClassificationListener].
 *
 * @property classifier Classifier to be used
 * @property listener receives the classified activities
 */
class ChainedClassifier(
    private val classifier: ActivityClassifier,
    private val listener: ActivityClassificationListener,
): TransformedSetReceiver {

    override fun onReceiveSet(set: SensorFrequenciesSet) {
        val result = classifier.classify(set)
        listener.onClassified(result)
    }

}