package de.tadris.recognizr.classifier

import de.tadris.recognizr.data.SensorFrequenciesSet
import de.tadris.recognizr.log.Logger
import org.pmml4s.model.Model
import kotlin.math.roundToInt

/**
 * Classifier using a PMML (Predictive Model Markup Language) Model
 *
 * @property model Model to be used
 */
class PMMLClassifier(val model: Model) : ActivityClassifier {

    companion object {

        private const val TAG = "PMMLClassifier"

        private const val MODEL_INPUTS = 128

    }

    override fun classify(input: SensorFrequenciesSet): ActivityType {
        val inputMap = input.magnitudes
            .asSequence()
            .take(MODEL_INPUTS) // only use first MODEL_INPUTS values
            .mapIndexed { index, value -> Pair("x${index + 1}", value) } // map to input names e.g. x1, x2, x3...
            .toMap()

        // Example output: {probability(cycling)=1.0, probability(walking)=1.87603775612871E-24}
        val outputMap = model.predict(inputMap)
        Logger.d(TAG, "Output: $outputMap")

        val activities = outputMap.entries
            .associate { Pair(convertOutputKeyToActivity(it.key), it.value as Double) }
            .filterValues { !it.isNaN() }

        if(activities.isEmpty()){
            Logger.w(TAG, "No activity recognised")
            return ActivityType.UNKNOWN
        }

        val msg = activities.entries
            .sortedBy { it.value }
            .joinToString(separator = "\n"){ "- ${it.key}: ${(it.value * 100).roundToInt()}%" }
        Logger.d(TAG, msg)

        val best = activities.maxBy { it.value }.key
        Logger.d(TAG, "Best guess: $best")

        return best
    }

    private fun convertOutputKeyToActivity(key: String): ActivityType {
        val id = key.replace("probability(", "").dropLast(1)
        return ActivityType.findById(id)
    }

}