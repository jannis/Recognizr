package de.tadris.recognizr.classifier

/**
 * Activities that can be classified.
 *
 * @property id String-ID
 * @property color Color Int
 * @property active if the activity is considered as physically 'active'
 */
enum class ActivityType(val id: String, val color: Int, val active: Boolean) {
    /**
     * Phone is not worn.
     */
    IDLE("idle", 0xff818181.toInt(), active = false),

    /**
     * User isn't doing an activity. e.g. sitting or standing.
     */
    INACTIVE("inactive", 0xff69a0c5.toInt(), active = false),

    /**
     * User is active but not doing a recognizable activity. e.g. chores, working.
     */
    ACTIVE("active", 0xff0072ff.toInt(), active = true),

    /**
     * User is walking.
     */
    WALKING("walking", 0xff54c900.toInt(), active = true),

    /**
     * User is running.
     */
    RUNNING("running", 0xff00a300.toInt(), active = true),

    /**
     * User is cycling.
     */
    CYCLING("cycling", 0xffca7ae8.toInt(), active = true),

    /**
     * An unknown activity was recognized.
     */
    UNKNOWN("unknown", 0xff818181.toInt(), active = false),

    ;

    companion object {

        /**
         * Finds an [ActivityType] by String-ID
         *
         * @param id ID
         * @return Found activity type, [UNKNOWN] if nothing was found
         */
        fun findById(id: String) = values().find { it.id == id } ?: UNKNOWN

    }
}