package de.tadris.recognizr.classifier

import de.tadris.recognizr.data.SensorFrequenciesSet

/**
 * Interface for an activity classifier
 */
interface ActivityClassifier {

    /**
     * Classifies the [ActivityType] from a given [SensorFrequenciesSet]
     *
     * @param input Input
     * @return classified [ActivityType]
     */
    fun classify(input: SensorFrequenciesSet): ActivityType

}