package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.SensorFrequenciesSet
import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.transform.DftNormalization
import org.apache.commons.math3.transform.FastFourierTransformer
import org.apache.commons.math3.transform.TransformType
import kotlin.math.sqrt

/**
 * Implements [FrequencyTransformation] using the [FastFourierTransformer] from the Apache commons library.
 */
object FastFourierTransformation : FrequencyTransformation {

    override fun transform(set: PackedSensorSet): SensorFrequenciesSet {
        val transformer = FastFourierTransformer(DftNormalization.STANDARD)
        val complex = transformer.transform(set.samples.map { it.acceleration }.toDoubleArray(), TransformType.FORWARD)
        return SensorFrequenciesSet(
            complex
                .map { it.magnitude } // Calculate magnitudes
                .dropLast(set.samples.size / 2) // Drop second half
                .toDoubleArray() // Convert list to array
        )
    }

    private val Complex.magnitude get() = sqrt(real * real + imaginary * imaginary)

}