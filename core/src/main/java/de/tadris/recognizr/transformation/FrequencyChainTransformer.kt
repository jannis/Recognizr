package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.SensorFrequenciesSet
import de.tadris.recognizr.recording.PackedSensorSetReceiver
import de.tadris.recognizr.recording.TransformedSetReceiver

/**
 * Receives packed sensor sets, transforms them and emits them to a [TransformedSetReceiver]
 * @param transformation Transformation to use
 * @param receiver Receiver that gets new [SensorFrequenciesSet]s
 * @see PackedSensorSetReceiver
 * @see FrequencyTransformation
 */
class FrequencyChainTransformer(
    private val transformation: FrequencyTransformation,
    private val receiver: TransformedSetReceiver,
): PackedSensorSetReceiver {

    override fun onReceiveSet(set: PackedSensorSet) {
        emit(set.transform())
    }

    private fun PackedSensorSet.transform() = transformation.transform(this)

    private fun emit(frequencySet: SensorFrequenciesSet){
        receiver.onReceiveSet(frequencySet)
    }

}