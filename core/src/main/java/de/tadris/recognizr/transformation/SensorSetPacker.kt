package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.RawSensorSet
import de.tadris.recognizr.data.SensorSample
import de.tadris.recognizr.data.TimedSensorSample
import de.tadris.recognizr.log.Logger

/**
 * Packs a RawSensorSet into a PacketSensorSet. This is done through linear resampling.
 *
 * @property set Input set
 * @throws IllegalArgumentException if duration of set is less than PackedSensorSet.DURATION_MICRO_SECONDS
 */
class SensorSetPacker(val set: RawSensorSet) {

    private val offset get() = set.start

    init {
        if(set.duration < PackedSensorSet.DURATION_MICRO_SECONDS)
            throw IllegalArgumentException("Set duration must be at least ${PackedSensorSet.DURATION_MICRO_SECONDS} us")
    }

    /**
     * Packs the RawSensorSet into a PacketSensorSet
     */
    fun pack() = PackedSensorSet(
        buildList {
            repeat(PackedSensorSet.SAMPLE_COUNT){ index ->
                val time = index * PackedSensorSet.SAMPLE_DURATION_MICRO_SECONDS
                val value = getValueAt(time)
                add(value)
            }
        }
    )

    private fun getValueAt(time: Long): SensorSample {
        val nearest = getNearest(time)
        // Logger.d(TAG, "nearest: $nearest")
        if(nearest.isEmpty()) throw Exception("Cannot find nearest samples")
        if(nearest.size == 1) return nearest.first().sample // Return only available sample
        return linearSampling(nearest.first(), nearest.last(), time)
    }

    /**
     * Returns a sample between first and second sample
     *
     * @param first first sample
     * @param second second sample
     * @param time resample time, ideally between first and second sample time
     * @return resampled value at given time
     */
    private fun linearSampling(first: TimedSensorSample, second: TimedSensorSample, time: Long): SensorSample {
        val diff = second.timestamp - first.timestamp

        if(diff == 0L) return first.sample // Samples at same time

        // factor how much the second value counts compared to first, ideally be between 0 and 1
        // 0 -> first | 0.5 -> in the middle | 1 -> second
        val factor = (time - first.normalizedTime).toDouble() / diff
        return SensorSample(
            factor * second.sample.acceleration + (1 - factor) * first.sample.acceleration
        )
    }

    private fun getNearest(time: Long): List<TimedSensorSample> {
        return when {
            time < set.set.first().normalizedTime -> listOf(set.set.first())
            time > set.set.last().normalizedTime -> listOf(set.set.last())
            else -> {
                val index = set.set.indexOfFirst { it.normalizedTime > time }.coerceIn(1, set.set.lastIndex)
                listOf(set.set[index - 1], set.set[index])
            }
        }
    }

    private val TimedSensorSample.normalizedTime get() = timestamp - offset

}