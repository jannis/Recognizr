package de.tadris.recognizr.transformation

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.SensorFrequenciesSet
import org.apache.commons.math3.transform.FastFourierTransformer

/**
 * Transforms a [PackedSensorSet] to their frequency magnitudes
 *
 */
interface FrequencyTransformation {

    /**
     * Transforms a [PackedSensorSet] using [FastFourierTransformer] to their frequency magnitudes
     *
     * @param set Set to be transformed
     * @return set containing frequency magnitudes
     */
    fun transform(set: PackedSensorSet): SensorFrequenciesSet

}