package de.tadris.recognizr.recording

import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.data.RawSensorSet
import de.tadris.recognizr.data.TimedSensorSample
import de.tadris.recognizr.log.Logger
import de.tadris.recognizr.transformation.SensorSetPacker

/**
 * Receives and caches [TimedSensorSample]s. As soon as enough data is recorded
 * ([PackedSensorSet.DURATION_MICRO_SECONDS]) it packs the data into a [PackedSensorSet] and emits it.
 *
 * @property packetSetReceiver Receiver which gets generated sets
 */
class SensorSetBuilder(private val packetSetReceiver: PackedSensorSetReceiver) : SensorDataListener {

    companion object {
        const val TAG = "SensorSetBuilder"
    }

    private val data = mutableListOf<TimedSensorSample>()

    /**
     * Time range of data in microseconds
     * @see data
     */
    private val timeRange get() =
        if(data.isEmpty()) 0L
        else data.last().timestamp - data.first().timestamp

    /**
     * Timestamp of last sample
     */
    private var lastTimestamp = 0L

    override fun receive(sample: TimedSensorSample) {
        if(sample.timestamp < lastTimestamp) {
            throw IllegalArgumentException("New timestamp (${sample.timestamp}) is smaller than previous: $lastTimestamp")
        }
        data += sample
        lastTimestamp = sample.timestamp
        checkSetComplete()
    }

    /**
     * Checks whether a new set is complete. A new set is complete if it's at least
     * [PackedSensorSet.DURATION_MICRO_SECONDS] long. If it's complete, it will be packed and emitted.
     */
    private fun checkSetComplete(){
        if(timeRange > PackedSensorSet.DURATION_MICRO_SECONDS){
            packAndEmitSet()
        }
    }

    /**
     * Packs a sensor set using [SensorSetPacker] and emits them to the specified [packetSetReceiver].
     */
    private fun packAndEmitSet(){
        Logger.d(TAG, "Packing new set from ${data.first().timestamp} to ${data.last().timestamp}")
        // Logger.d(TAG, "Raw: $data")
        val packed = SensorSetPacker(buildRawSet()).pack()
        // Logger.d(TAG, "Packed: $packed")
        packetSetReceiver.onReceiveSet(packed)
        data.clear()
        lastTimestamp = 0L
    }

    private fun buildRawSet() = RawSensorSet(data.toList())

}