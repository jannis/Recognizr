package de.tadris.recognizr.recording
import de.tadris.recognizr.data.SensorFrequenciesSet

/**
 * An interface listening to new [SensorFrequenciesSet]
 */
interface TransformedSetReceiver {

    /**
     * A new set of frequency magnitudes is available
     *
     * @param set [SensorFrequenciesSet]
     */
    fun onReceiveSet(set: SensorFrequenciesSet)

}