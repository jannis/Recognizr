package de.tadris.recognizr.recording

import de.tadris.recognizr.data.TimedSensorSample

/**
 * An interface listening for new timed samples.
 * @see TimedSensorSample
 */
fun interface SensorDataListener {

    /**
     * Called when a new sample is available. The timestamp should be ascending.
     * @param sample new sample
     * @throws IllegalArgumentException if the timestamp descends (not allowed) or the timestamp is negative
     */
    fun receive(sample: TimedSensorSample)

}