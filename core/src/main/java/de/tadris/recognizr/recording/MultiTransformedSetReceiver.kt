package de.tadris.recognizr.recording

import de.tadris.recognizr.data.SensorFrequenciesSet

/**
 * Receives [SensorFrequenciesSet]s and gives them to multiple [TransformedSetReceiver].
 *
 * @property receivers Receivers that should be informed about new sets
 */
class MultiTransformedSetReceiver(private val receivers: List<TransformedSetReceiver>): TransformedSetReceiver {

    override fun onReceiveSet(set: SensorFrequenciesSet) {
        receivers.forEach {
            it.onReceiveSet(set)
        }
    }

}