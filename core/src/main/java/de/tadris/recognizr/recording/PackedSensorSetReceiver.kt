package de.tadris.recognizr.recording

import de.tadris.recognizr.data.PackedSensorSet

/**
 * An interface listening to [PackedSensorSet]s
 */
interface PackedSensorSetReceiver {

    /**
     * A new set is available
     *
     * @param set new set
     */
    fun onReceiveSet(set: PackedSensorSet)

}