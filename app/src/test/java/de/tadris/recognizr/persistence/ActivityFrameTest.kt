package de.tadris.recognizr.persistence

import de.tadris.recognizr.classifier.ActivityType
import org.junit.Assert.*
import org.junit.Test

class ActivityFrameTest {

    @Test
    fun testGetType(){
        val frame1 = ActivityFrame(123, ActivityType.WALKING.id, 0L, 1000L)
        assertEquals(ActivityType.WALKING, frame1.type)

        val frame2 = ActivityFrame(123, "sfjdkhdfks", 0L, 1000L)
        assertEquals(ActivityType.UNKNOWN, frame2.type)
    }

    @Test
    fun testDuration(){
        val frame1 = ActivityFrame(123, ActivityType.WALKING.id, 10200L, 15200L)
        assertEquals(5000L, frame1.duration)
    }

}