package de.tadris.recognizr.persistence

import de.tadris.recognizr.classifier.ActivityType
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock

class ActivityFrameAggregatorTest {

    @Test
    fun testAggregator(){
        val minute = 1000L * 60 // 1 minute in millis
        val dao: FrontendActivityFrameDao = mock {
            on { findAllByTimestampAfter(any()) }.thenAnswer {
                listOf(
                    ActivityFrame(1, ActivityType.WALKING.id, 1 * minute, 2 * minute),
                    ActivityFrame(2, ActivityType.WALKING.id, 2 * minute, 3 * minute),
                    ActivityFrame(3, ActivityType.CYCLING.id, 3 * minute, 5 * minute),
                    ActivityFrame(4, ActivityType.INACTIVE.id, 5 * minute, 6 * minute),
                )
            }
        }

        val aggregator = ActivityFrameAggregator(dao)

        val result = aggregator.sumDurationsAfter(0L)
        assertEquals(2 * minute, result[ActivityType.WALKING])
        assertEquals(2 * minute, result[ActivityType.CYCLING])
        assertEquals(1 * minute, result[ActivityType.INACTIVE])
        assertEquals(4, aggregator.getActiveMinutesAfter(0L))
    }

}