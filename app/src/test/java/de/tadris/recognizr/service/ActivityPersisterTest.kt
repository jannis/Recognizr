package de.tadris.recognizr.service

import de.tadris.recognizr.classifier.ActivityType
import de.tadris.recognizr.persistence.ActivityFrame
import de.tadris.recognizr.persistence.BackendActivityFrameDao
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class ActivityPersisterTest {

    @Test
    fun testPersister(){
        val mockDb = mutableListOf<ActivityFrame>()
        val dao = object : BackendActivityFrameDao {
            override fun findLast(): ActivityFrame? {
                println("findLast")
                return mockDb.lastOrNull()
            }

            override fun update(activityFrame: ActivityFrame) {
                println("update $activityFrame")
            }

            override fun insert(activityFrame: ActivityFrame) {
                println("insert $activityFrame")
                mockDb += activityFrame
            }
        }


        val persister = ActivityPersister(dao)
        persister.onClassified(ActivityType.WALKING)

        assertEquals(1, mockDb.size) // verify insertion
        assertEquals(ActivityType.WALKING, mockDb.first().type) // verify correct type
        val endTime = mockDb.first().endTimestamp

        Thread.sleep(10) // simulate time

        persister.onClassified(ActivityType.WALKING)
        assertNotEquals(endTime, mockDb.first().endTimestamp) // verify end time updated

        persister.onClassified(ActivityType.ACTIVE)
        assertEquals(1, mockDb.size) // verify no further insertion
        persister.onClassified(ActivityType.ACTIVE)
        assertEquals(1, mockDb.size) // verify no further insertion
        persister.onClassified(ActivityType.ACTIVE)

        assertEquals(2, mockDb.size) // verify second insertion
        assertEquals(ActivityType.ACTIVE, mockDb.last().type)

    }

}