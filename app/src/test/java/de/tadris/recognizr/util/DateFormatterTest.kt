package de.tadris.recognizr.util

import org.junit.Assert.*
import org.junit.Test
import java.util.TimeZone

class DateFormatterTest {

    @Test
    fun testFormatDate(){
        // dd.MM.yyyy HH:mm
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
        assertEquals("01.01.1970 00:00", DateFormatter.formatFor(0L))
        assertEquals("17.01.2023 12:46", DateFormatter.formatFor(1673959617025L))
    }

}