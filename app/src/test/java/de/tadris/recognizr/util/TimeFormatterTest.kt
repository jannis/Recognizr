package de.tadris.recognizr.util

import org.junit.Assert.*
import org.junit.Test

class TimeFormatterTest {

    @Test
    fun testGetHoursMinutes(){
        assertEquals("0:00", TimeFormatter.getHoursMinutes(0))
        assertEquals("0:20", TimeFormatter.getHoursMinutes(20))
        assertEquals("1:09", TimeFormatter.getHoursMinutes(69))
    }

}