package de.tadris.recognizr.util

/**
 * Object providing utils for time formatting
 */
object TimeFormatter {

    /**
     * Returns hours and minutes in the format "HH:MM"
     *
     * @param minutes Total minutes
     * @return formatted String
     */
    fun getHoursMinutes(minutes: Long): String {
        val hours = minutes / 60
        val rest = minutes % 60
        return "%d:%02d".format(hours, rest)
    }

}