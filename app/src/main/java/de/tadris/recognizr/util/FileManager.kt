package de.tadris.recognizr.util

import android.content.Context
import androidx.core.content.FileProvider
import de.tadris.recognizr.BuildConfig.APPLICATION_ID
import java.io.File
import java.io.IOException

/**
 * Object providing utils for providing sharable files in Android.
 */
object FileManager {

    private const val SHARED_DIRECTORY_NAME = "shared"
    private const val AUTHORITY_ID = "$APPLICATION_ID.fileprovider"

    /**
     * Creates a [File] with given filename that can be shared.
     *
     * @param context Android context
     * @param filename Name of the new file. Must not be empty
     * @throws IllegalArgumentException if filename is empty
     * @throws IOException if the parent directories could not be created
     * @return File to be shared later
     */
    fun createSharableFile(context: Context, filename: String): File {
        if(filename.isEmpty()) throw IllegalArgumentException("Filename is empty")
        val filePath = getSharedDirectory(context) + "/" + filename
        val file = File(filePath)
        val parent = file.parentFile
        if (parent != null) {
            if (!parent.exists() && !parent.mkdirs()) {
                throw IOException("Cannot create parent directories of $file")
            }
        }
        return file
    }

    private fun getSharedDirectory(context: Context) =
        context.filesDir.absolutePath + "/" + SHARED_DIRECTORY_NAME

    /**
     * Provides an access URI for a sharable file that can be used to share with other apps.
     * The file should be created using [createSharableFile]
     *
     * @param context Android context
     * @param file File to be shared
     * @return URI to be shared
     * @throws IllegalArgumentException if file cannot be provided.
     * @see createSharableFile
     */
    fun provide(context: Context, file: File) =
        FileProvider.getUriForFile(context, AUTHORITY_ID, file)

}