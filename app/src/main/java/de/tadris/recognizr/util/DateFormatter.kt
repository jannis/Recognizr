package de.tadris.recognizr.util

import java.text.SimpleDateFormat

/**
 * Object providing utils for date formatting
 */
object DateFormatter {

    private val date = SimpleDateFormat("dd.MM.yyyy HH:mm")

    /**
     * Formats a timestamp
     * @return Formatted time
     */
    fun formatFor(timestamp: Long) = date.format(timestamp)!!

}