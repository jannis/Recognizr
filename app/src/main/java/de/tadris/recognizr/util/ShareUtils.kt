package de.tadris.recognizr.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import de.tadris.recognizr.R
import org.apache.commons.io.FileUtils
import java.io.File

/**
 * Object providing utils to share files
 */
object ShareUtils {

    private const val TAG = "ShareUtils"

    /**
     * Creates a sharable file from a non-sharable by copying the source file and opens the share intent.
     *
     * @param context Android context
     * @param sourceFile Source file to be shared
     * @param name File name of shared file
     */
    fun shareFile(context: Context, sourceFile: File, name: String){
        val sharedFile = FileManager.createSharableFile(context, name)
        FileUtils.copyFile(sourceFile, sharedFile)
        val uri = FileManager.provide(context, sharedFile)
        openShareIntent(context, uri)
    }

    private fun openShareIntent(context: Context, fileUri: Uri){
        val intentShareFile = Intent(Intent.ACTION_SEND)
        intentShareFile.setDataAndType(fileUri, context.contentResolver.getType(fileUri))
        intentShareFile.putExtra(Intent.EXTRA_STREAM, fileUri)
        intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        context.startActivity(Intent.createChooser(intentShareFile, context.getString(R.string.shareFileTitle)))

        Log.d(TAG, "Sharing file $fileUri")
        Log.d(TAG, "File Type: ${context.contentResolver.getType(fileUri)}")
    }

}