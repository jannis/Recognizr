package de.tadris.recognizr.util

import android.Manifest
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.ActivityCompat
import de.tadris.recognizr.R
import java.security.Permissions

/**
 * Object providing utils for notification channels
 */
object NotificationChannelHelper {

    /**
     * Notification-ID used by the CollectorService
     */
    const val COLLECTOR_NOTIFICATION_ID = 1

    /**
     * Channel-ID of service info messages
     */
    const val CHANNEL_SERVICE = "serviceInfo"

    fun requestPermission(activity: Activity) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) return
        val notificationManager = activity.getSystemService(NotificationManager::class.java)
        if(!notificationManager.areNotificationsEnabled()){
            ActivityCompat.requestPermissions(activity, arrayOf(
                Manifest.permission.POST_NOTIFICATIONS
            ), 1)
        }
    }

    /**
     * Creates notification channels if above Android 8 (Oreo)
     *
     * @param context Android context
     */
    fun createChannels(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(
                context,
                CHANNEL_SERVICE,
                R.string.channelServiceTitle,
                R.string.channelServiceDescription,
                NotificationManager.IMPORTANCE_LOW
            )
        }
    }

    private fun createNotificationChannel(context: Context, id: String, nameId: Int, descriptionId: Int, importance: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(nameId)
            val channel = NotificationChannel(id, name, importance)
            channel.description = context.getString(descriptionId)

            val notificationManager = context.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

}