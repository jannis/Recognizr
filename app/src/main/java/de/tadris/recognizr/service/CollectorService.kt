package de.tadris.recognizr.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import de.tadris.recognizr.ui.MainActivity
import de.tadris.recognizr.util.NotificationChannelHelper
import de.tadris.recognizr.R
import de.tadris.recognizr.classifier.ActivityClassificationListener
import de.tadris.recognizr.classifier.ActivityType
import de.tadris.recognizr.classifier.ChainedClassifier
import de.tadris.recognizr.classifier.PMMLClassifier
import de.tadris.recognizr.persistence.DataRepository
import de.tadris.recognizr.recording.MultiTransformedSetReceiver
import de.tadris.recognizr.recording.SensorDataListener
import de.tadris.recognizr.recording.SensorSetBuilder
import de.tadris.recognizr.serialization.CsvSerializerFactory
import de.tadris.recognizr.serialization.FileSerializer
import de.tadris.recognizr.transformation.FrequencyChainTransformer
import org.pmml4s.model.Model
import java.io.File
import java.util.concurrent.Executors

/**
 * A service orchestrating and connecting the different components for collecting, preprocessing,
 * classifying and persisting the data.
 *
 */
class CollectorService : Service(), ActivityClassificationListener {

    companion object {

        private const val TAG = "DebugCollectorService"

        /**
         * File name into which debug data is written.
         */
        private const val COLLECTOR_OUTPUT_FILE = "collector.txt"

        /**
         * A variable for checking whether the service runs.
         */
        var isRunning = false
            private set

        /**
         * Starts the CollectorService if not running
         *
         * @param context Android context
         */
        fun startService(context: Context){
            if(!isRunning){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(getServiceIntent(context))
                }else{
                    context.startService(getServiceIntent(context))
                }
                isRunning = true
            }
        }

        /**
         * Stops the CollectorService if running
         *
         * @param context Android context
         */
        fun stopService(context: Context){
            if(isRunning){
                context.stopService(getServiceIntent(context))
                isRunning = false
            }
        }

        /**
         * Creates a CollectorService Intent to be started or stopped
         *
         * @param context Android context
         * @return CollectorService Intent
         */
        private fun getServiceIntent(context: Context) = Intent(context, CollectorService::class.java)

        /**
         * Returns the file into which debug data is written.
         *
         * @param context Android context
         * @return Debug file
         */
        fun getFile(context: Context) = File(context.filesDir, COLLECTOR_OUTPUT_FILE)

    }

    private lateinit var receiverManager: SensorReceiverManager
    private lateinit var chain: CollectorChain

    private var lastClassification: ActivityType? = null

    override fun onCreate() {
        Log.d(TAG, "onCreate")
        super.onCreate()

        chain = CollectorChain(this, this)
        receiverManager = SensorReceiverManager(this, chain.buildChain())
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        receiverManager.start() // Start receiving sensor data
        isRunning = true

        startForeground(NotificationChannelHelper.COLLECTOR_NOTIFICATION_ID, buildNotification())

        return START_STICKY
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        chain.destroy()
        receiverManager.stop()
        isRunning = false
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onClassified(activity: ActivityType) {
        Log.d(TAG, "Recognized: $activity")
        lastClassification = activity
        postNotification()
    }

    /**
     * Reposts the service notification
     */
    private fun postNotification(){
        getSystemService(NotificationManager::class.java)
            .notify(NotificationChannelHelper.COLLECTOR_NOTIFICATION_ID, buildNotification())
    }

    /**
     * Build the displayed service notification including the current activity type
     * @return [Notification] that can be posted
     */
    private fun buildNotification(): Notification {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)
        return NotificationCompat.Builder(this, NotificationChannelHelper.CHANNEL_SERVICE)
            .setContentTitle(getString(R.string.notificationCollectorTitle))
            .setContentText(lastClassification?.name ?: getString(R.string.notificationCollectorText))
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)
            .build()
    }

}