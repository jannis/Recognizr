package de.tadris.recognizr.service

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.util.Log
import de.tadris.recognizr.data.TimedSensorSample
import de.tadris.recognizr.recording.SensorDataListener

/**
 * Manages a [SensorReceiver]
 *
 * @param context Android context
 * @param dataListener Listener to receive new [TimedSensorSample]s
 */
class SensorReceiverManager(context: Context, dataListener: SensorDataListener) {

    companion object {
        const val TAG = "SensorReceiverManager"
    }

    private val sensorManager = context.getSystemService(SensorManager::class.java)
    private val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)
    private val sensorReceiver = SensorReceiver(dataListener)

    /**
     * Start receiving sensor data
     */
    fun start() {
        Log.d(TAG, "Registering sensor")
        sensorManager.registerListener(sensorReceiver, sensor, SensorManager.SENSOR_DELAY_GAME)
    }

    /**
     * Stop receiving sensor data
     */
    fun stop() {
        Log.d(TAG, "Unregistering sensor")
        sensorManager.unregisterListener(sensorReceiver)
    }

}