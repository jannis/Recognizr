package de.tadris.recognizr.service

import android.content.Context
import android.content.res.Resources
import android.util.Log
import de.tadris.recognizr.R
import de.tadris.recognizr.classifier.ActivityClassificationListener
import de.tadris.recognizr.classifier.ChainedClassifier
import de.tadris.recognizr.classifier.PMMLClassifier
import de.tadris.recognizr.persistence.DataRepository
import de.tadris.recognizr.recording.MultiTransformedSetReceiver
import de.tadris.recognizr.recording.SensorDataListener
import de.tadris.recognizr.recording.SensorSetBuilder
import de.tadris.recognizr.serialization.CsvSerializerFactory
import de.tadris.recognizr.serialization.FileSerializer
import de.tadris.recognizr.transformation.FastFourierTransformation
import de.tadris.recognizr.transformation.FrequencyChainTransformer
import org.pmml4s.model.Model
import java.util.concurrent.Executors

class CollectorChain(private val context: Context, private val listener: ActivityClassificationListener) {

    private val backgroundExecutor = Executors.newSingleThreadExecutor()

    /**
     * Builds the processing chain. It creates instances of different components and
     * connects them together.
     *
     * @return a [SensorDataListener] that receives initial input
     */
    fun buildChain(): SensorDataListener {
        // Build chain:
        //
        //     SensorEvent          TimedSensorSample     PackedSensorSet
        //          |                        |                   |
        // (Sensor) -> SensorReceiverManager -> SensorSetBuilder -> ...
        //
        //
        //
        //                    SensorFrequenciesSet     serialized Data
        //                                  |                 |
        // ... -> FrequencyChainTransformer -> FileSerializer -> (output file)
        //                                  \
        //                                   \-> Classifier -> ActivityPersister <-> (Database)
        //                                                  |                     |
        //                                             ActivityType         ActivityFrame
        //
        val serializer = FileSerializer(CollectorService.getFile(context), CsvSerializerFactory)

        val db = DataRepository.getInstance(context).db
        val persister = ActivityPersister(db.backendFrameDao())

        val modelStream = context.resources.openRawResource(R.raw.model)
        val model = Model.fromInputStream(modelStream)
        val classifier = PMMLClassifier(model)
        val classifierChain = ChainedClassifier(classifier){
            listener.onClassified(it)
            backgroundExecutor.submit {
                persister.onClassified(it)
            }
        }

        val transformedSetReceiver = MultiTransformedSetReceiver(listOf(
            serializer,
            classifierChain
        ))
        val transformer = FrequencyChainTransformer(FastFourierTransformation, transformedSetReceiver)
        return SensorSetBuilder(transformer)
    }

    fun destroy(){
        backgroundExecutor.shutdown()
    }

}