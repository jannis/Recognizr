package de.tadris.recognizr.service

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.util.Log
import de.tadris.recognizr.data.SensorSample
import de.tadris.recognizr.data.TimedSensorSample
import de.tadris.recognizr.recording.SensorDataListener
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Receives SensorEvents and generates [TimedSensorSample] which are given to the [SensorDataListener].
 *
 * @property dataListener Data Listener to receive [TimedSensorSample]s
 */
class SensorReceiver(private val dataListener: SensorDataListener): SensorEventListener {

    companion object {
        const val TAG = "SensorReceiver"
    }

    /**
     * Time of first sensor sample. Used to calculate relative times.
     */
    private var firstTime = -1L

    override fun onSensorChanged(event: SensorEvent?) {
        event ?: return // ignore null events
        val timestamp = event.timestamp / 1000L // nanosecond -> microsecond
        val x = event.values[0].toDouble()
        val y = event.values[1].toDouble()
        val z = event.values[2].toDouble()
        val acceleration = sqrt(x.pow(2) + y.pow(2) + z.pow(2)) // absolute acceleration

        // Log.d(TAG, "Sensor event at $timestamp: $acceleration")

        val sample = TimedSensorSample(
            getRelativeTime(timestamp),
            SensorSample(acceleration)
        )

        emit(sample)
    }

    /**
     * Emits a new sample
     *
     * @param sample Sample to be emitted
     */
    private fun emit(sample: TimedSensorSample){
        dataListener.receive(sample)
    }

    /**
     * Calculates relative time to start from an absolute time
     *
     * @param absoluteTime Absolute time in us
     * @return Relative time in us
     */
    private fun getRelativeTime(absoluteTime: Long): Long {
        if(firstTime == -1L) firstTime = absoluteTime // first sensor time is time offset
        return absoluteTime - firstTime
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        Log.d(TAG, "accuracy changed: $accuracy")
    }
}