package de.tadris.recognizr.service

import android.util.Log
import de.tadris.recognizr.classifier.ActivityClassificationListener
import de.tadris.recognizr.classifier.ActivityType
import de.tadris.recognizr.data.PackedSensorSet
import de.tadris.recognizr.persistence.ActivityFrame
import de.tadris.recognizr.persistence.BackendActivityFrameDao

/**
 * Persists data using a DAO following the rules:
 * - new activities are directly inserted into the database
 * - the current activity can only be changed if there are [REQUIRED_CLASSIFICATIONS_IN_A_ROW] consecutive recognitions of the same activity
 *
 * @property dao DAO providing access to the persistence layer
 */
class ActivityPersister(private val dao: BackendActivityFrameDao): ActivityClassificationListener {

    companion object {

        const val TAG = "ActivityPersister"

        /**
         * Interval in which new activity types are recognized typically.
         * in milliseconds
         */
        const val RECOGNITION_INTERVAL = (PackedSensorSet.DURATION_SECONDS * 1000).toLong()

        /**
         * At least this number of consecutive recognitions of the same type are required to change
         * the activity.
         */
        const val REQUIRED_CLASSIFICATIONS_IN_A_ROW = 3

        /**
         * If the time between two activity recognition is greater than this number, the activity
         * will be split into two different frames.
         *
         * in milliseconds
         */
        const val TIME_BETWEEN_FRAMES = 1000L * 60 * 60 * 3

    }

    /**
     * List of last recognized activities. Should be at most [REQUIRED_CLASSIFICATIONS_IN_A_ROW] long.
     */
    private val lastActivities = mutableListOf<ActivityType>()

    override fun onClassified(activity: ActivityType) {
        Log.d(TAG, "Received new classification: $activity")
        appendToLastActivities(activity)

        val last = dao.findLast()
        if(last != null && time() - last.endTimestamp < TIME_BETWEEN_FRAMES){
            if(last.type == activity){
                Log.d(TAG, "Extending frame")
                // last activity frame can be extended
                extendLastFrame(last)
            }else if(canChangeType()){
                Log.d(TAG, "Changing type")
                // change activity type
                changeActivityType(last, activity)
            }else{
                Log.d(TAG, "Type cannot be changed yet, doing nothing")
                // nothing can be done
            }
        }else{
            Log.d(TAG, "No frame found, inserting new one")
            insertNewFrame(activity)
        }
    }

    /**
     * Extends last activity frame to current time
     * @param last [ActivityFrame] to be extended
     */
    private fun extendLastFrame(last: ActivityFrame){
        last.endTimestamp = time()
        dao.update(last)
    }

    /**
     * Changes the activity type.
     *
     * @param last Last activity frame. The end time will be set to the start time of the new frame.
     * @param newType New [ActivityType]
     */
    private fun changeActivityType(last: ActivityFrame, newType: ActivityType){
        val newFrame = insertNewFrame(newType)

        // Update end time of last to connect to the new one
        last.endTimestamp = newFrame.startTimestamp
        dao.update(last)
    }

    /**
     * Creates a new activity frame of the current type and inserts it.
     *
     * @param activity new activity type
     * @return created frame
     */
    private fun insertNewFrame(activity: ActivityType): ActivityFrame {
        // Start is now minus REQUIRED_CLASSIFICATIONS_IN_A_ROW recognition intervals
        val start = time() - REQUIRED_CLASSIFICATIONS_IN_A_ROW * RECOGNITION_INTERVAL
        val end = time()
        val frame = ActivityFrame(time(), activity.id, start, end)
        dao.insert(frame)
        return frame
    }

    /**
     * Checks whether the activity type can be changed. The following conditions have to be met:
     * - at least [REQUIRED_CLASSIFICATIONS_IN_A_ROW] classifications
     * - last [REQUIRED_CLASSIFICATIONS_IN_A_ROW] classifications are equal
     * @return whether it's allowed to change the activity type
     */
    private fun canChangeType(): Boolean {
        if(lastActivities.size < REQUIRED_CLASSIFICATIONS_IN_A_ROW) return false
        return checkLastActivitiesEqual()
    }

    /**
     * Checks whether the gathered types in [lastActivities] are equal
     * @return if all activities in [lastActivities] are equal
     */
    private fun checkLastActivitiesEqual(): Boolean {
        val first = lastActivities.first()
        for(otherIdx in 1 until lastActivities.size){
            if(first != lastActivities[otherIdx]) return false
        }
        return true
    }

    /**
     * Appends a recognized activity type into the [lastActivities] list and cycles old out so the
     * maximum size is [REQUIRED_CLASSIFICATIONS_IN_A_ROW]
     *
     * @param activity Recognized [ActivityType]
     */
    private fun appendToLastActivities(activity: ActivityType){
        lastActivities += activity
        if(lastActivities.size > REQUIRED_CLASSIFICATIONS_IN_A_ROW){
            lastActivities.removeFirst()
        }
    }

    /**
     * Generates a timestamp of the current time
     *
     * @return current timestamp (UNIX-time)
     */
    private fun time() = System.currentTimeMillis()

}