package de.tadris.recognizr.persistence

import de.tadris.recognizr.classifier.ActivityType

/**
 * Class that aggregates [ActivityFrame]s from the database
 *
 * @property dao DAO to access the data source
 */
class ActivityFrameAggregator(private val dao: FrontendActivityFrameDao) {

    /**
     * Calculates the active minutes after a certain start time. An activity frame is considered as
     * active if the [ActivityType] is declared as active.
     *
     * @param begin Start time
     * @return active minutes
     */
    fun getActiveMinutesAfter(begin: Long) =
        sumDurationsAfter(begin)
            .filterKeys { it.active } // Only take active
            .values // Get values in milliseconds
            .sum() / 1000 / 60 // Sum and convert millis -> minutes

    /**
     * Calculates the sum of durations per [ActivityType] since a starting timestamp
     *
     * @param begin start timestamp
     * @return duration by type in milliseconds
     */
    fun sumDurationsAfter(begin: Long): Map<ActivityType, Long> {
        val output = mutableMapOf<ActivityType, Long>()

        dao.findAllByTimestampAfter(begin).forEach { frame ->
            output.addDuration(frame.type, frame.duration)
        }

        return output
    }

    private fun <T> MutableMap<T, Long>.addDuration(key: T, duration: Long){
        val oldValue = getOrElse(key){ 0L }
        val newValue = oldValue + duration
        this[key] = newValue
    }

}