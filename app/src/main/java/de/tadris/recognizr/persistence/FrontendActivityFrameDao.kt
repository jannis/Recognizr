package de.tadris.recognizr.persistence

import androidx.room.Dao
import androidx.room.Query

/**
 * Interface for accessing [ActivityFrame]s in the backend
 */
@Dao
interface FrontendActivityFrameDao {

    /**
     * Returns a list of all [ActivityFrame]s sorted by their end time descending.
     *
     * @return List of all [ActivityFrame]s
     */
    @Query("select * from activity_frame order by `end` desc")
    fun findAll(): List<ActivityFrame>

    /**
     * Returns a list of all [ActivityFrame]s that begin after a certain timestamp
     */
    @Query("select * from activity_frame where start > :min")
    fun findAllByTimestampAfter(min: Long): List<ActivityFrame>

}