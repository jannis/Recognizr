package de.tadris.recognizr.persistence

import android.content.Context
import android.os.Build
import androidx.room.Room
import de.tadris.recognizr.BuildConfig
import de.tadris.recognizr.classifier.ActivityType

/**
 * Singleton class holding access to the database
 *
 * @property db Reference to the [AppDatabase] and DAOs
 */
class DataRepository private constructor(val db: AppDatabase){

    constructor(context: Context): this(
        Room.databaseBuilder(context, AppDatabase::class.java, "activities.db").build()
    )

    companion object {
        private var instance: DataRepository? = null

        /**
         * Returns a [DataRepository] instance
         *
         * @param context Android context
         * @return [DataRepository] instance
         */
        fun getInstance(context: Context) = instance ?: kotlin.run {
            instance = DataRepository(context)
            instance!!
        }

        /**
         * Injects a mock database
         *
         * @param database Mocked database
         * @throws IllegalStateException if the Android app was built in release configuration
         */
        fun injectMock(database: AppDatabase){
            if(!BuildConfig.DEBUG) throw IllegalStateException("injectMock is only allowed in debug builds")
            instance = DataRepository(database)
        }

        /**
         * Removes a mock database
         */
        fun clearMock(){
            if(!BuildConfig.DEBUG) throw IllegalStateException("clearMock is only allowed in debug builds")
            instance = null
        }
    }

}