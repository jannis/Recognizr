package de.tadris.recognizr.persistence

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * Gives access to the database via Android Room
 */
@Database(entities = [ActivityFrame::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun backendFrameDao(): BackendActivityFrameDao

    abstract fun frontendFrameDao(): FrontendActivityFrameDao

}