package de.tadris.recognizr.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

/**
 * Interface for accessing and modifying [ActivityFrame]s in the backend
 */
@Dao
interface BackendActivityFrameDao {

    /**
     * Finds latest activity frame by end time
     *
     * @return latest ActivityFrame or null if there is none
     */
    @Query("select * from activity_frame order by `end` desc limit 1")
    fun findLast(): ActivityFrame?

    /**
     * Updates an existing ActivityFrame
     *
     * @param activityFrame ActivityFrame to be updated
     */
    @Update
    fun update(activityFrame: ActivityFrame)

    /**
     * Inserts a new ActivityFrame
     *
     * @param activityFrame ActivityFrame to be inserted
     */
    @Insert
    fun insert(activityFrame: ActivityFrame)

}