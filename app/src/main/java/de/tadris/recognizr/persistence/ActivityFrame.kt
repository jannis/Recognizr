package de.tadris.recognizr.persistence

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import de.tadris.recognizr.classifier.ActivityType

/**
 * A data class representing a performed activity within a certain time frame.
 *
 * @property id Unique identifier
 * @property typeId ID of the activity type
 * @property startTimestamp UNIX-Timestamp of the start time
 * @property endTimestamp UNIX-Timestamp of the end time
 *
 * @see ActivityType
 */
@Entity(tableName = "activity_frame", indices = [
    Index(value = ["end"], orders = [Index.Order.DESC])
])
data class ActivityFrame(
    @PrimaryKey var id: Long,
    @ColumnInfo(name = "type_id") val typeId: String,
    @ColumnInfo(name = "start") val startTimestamp: Long,
    @ColumnInfo(name = "end") var endTimestamp: Long,
){

    /**
     * [ActivityType] getter
     */
    val type get() = ActivityType.findById(typeId)

    /**
     * Duration of the activity frame in milliseconds
     */
    val duration get() = endTimestamp - startTimestamp

}