package de.tadris.recognizr.ui

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * Adapter for a ViewPager containing the two statics tabs.
 */
class StatisticsTabAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {

    companion object {
        const val POSITION_OVERVIEW = 0
        const val POSITION_HISTORY = 1
    }

    override fun getItemCount() = 2

    override fun createFragment(position: Int) = when(position) {
        POSITION_OVERVIEW -> OverviewFragment()
        POSITION_HISTORY -> HistoryFragment()
        else -> throw IllegalArgumentException("Cannot create fragment at unknown position $position")
    }
}