package de.tadris.recognizr.ui

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.tadris.recognizr.persistence.ActivityFrameAggregator
import de.tadris.recognizr.persistence.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

/**
 * ViewModel for [OverviewFragment]
 *
 */
class OverviewViewModel : ViewModel() {

    companion object {

        private const val TAG = "OverviewFragment"
        val PIE_CHART_DURATION = TimeUnit.DAYS.toMillis(1)

    }

    private val _chartState = MutableLiveData(OverviewChartState(emptyMap()))
    private val _infoState = MutableLiveData(OverviewInfoState(0L))

    val chartState: LiveData<OverviewChartState> = _chartState
    val infoState: LiveData<OverviewInfoState> = _infoState

    fun load(context: Context){
        viewModelScope.launch {
            _chartState.postValue(OverviewChartState(durations = fetchDurationsAfter(context, getBegin())))
            _infoState.postValue(OverviewInfoState(activeMinutes = getActiveMinutesAfter(context, getBegin())))
            Log.d(TAG, "Posted new chart & info state")
        }
    }

    private fun getBegin() = System.currentTimeMillis() - PIE_CHART_DURATION

    private suspend fun fetchDurationsAfter(context: Context, begin: Long) = withContext(Dispatchers.IO){
        getAggregator(context).sumDurationsAfter(begin)
    }

    private suspend fun getActiveMinutesAfter(context: Context, begin: Long) = withContext(Dispatchers.IO){
        getAggregator(context).getActiveMinutesAfter(begin)
    }

    private fun getAggregator(context: Context) =
        ActivityFrameAggregator(DataRepository.getInstance(context).db.frontendFrameDao())

}