package de.tadris.recognizr.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import de.tadris.recognizr.R
import de.tadris.recognizr.persistence.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Activity showing statistics. Contains a view pager with the 2 tabs:
 * - [OverviewFragment]
 * - [HistoryFragment]
 *
 */
class StatisticsActivity : AppCompatActivity() {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)
        setTitle(R.string.titleStatistics)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tabLayout = findViewById(R.id.statisticsTabLayout)
        viewPager = findViewById(R.id.statisticsViewPager)

        viewPager.adapter = StatisticsTabAdapter(this)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = getString(when(position){
                StatisticsTabAdapter.POSITION_OVERVIEW -> R.string.statisticsOverview
                StatisticsTabAdapter.POSITION_HISTORY -> R.string.statisticsHistory
                else -> throw IllegalArgumentException("Cannot find name for tab at position $position")
            })
        }.attach()
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when(item.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

}