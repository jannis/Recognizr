package de.tadris.recognizr.ui

import de.tadris.recognizr.classifier.ActivityType

/**
 * State of the chart in the overview fragment.
 * @property durations Maps the type to the duration sums over a given time range
 */
data class OverviewChartState(val durations: Map<ActivityType, Long>)