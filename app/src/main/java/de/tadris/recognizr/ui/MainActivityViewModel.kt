package de.tadris.recognizr.ui

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import de.tadris.recognizr.R
import de.tadris.recognizr.service.CollectorService
import de.tadris.recognizr.util.ShareUtils
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update

/**
 * ViewModel for [MainActivity]
 *
 */
class MainActivityViewModel : ViewModel() {

    companion object {
        private const val TAG = "MainActivityViewModel"
    }

    private val _serviceState = MutableStateFlow(isServiceRunning())

    /**
     * If the service is currently started
     */
    val serviceState: StateFlow<Boolean> = _serviceState

    /**
     * Toggles the service state: Starts service of stopped, stops service if started.
     *
     * @param context
     */
    fun toggleService(context: Context){
        if (isServiceRunning()) {
            Log.d(TAG, "Stopping service")
            CollectorService.stopService(context)
        } else {
            Log.d(TAG, "Starting service")
            CollectorService.startService(context)
        }
        updateServiceState()
    }

    private fun updateServiceState(){
        _serviceState.update { isServiceRunning() }
    }

    /**
     * Triggers the share dialog for the debug file
     *
     * @param context
     */
    fun shareCollectedLogs(context: Context){
        Log.d(TAG, "Sharing collected debug logs")
        val file = CollectorService.getFile(context)
        ShareUtils.shareFile(context, file, "data.txt")
    }

    /**
     * Deletes the debug log file
     *
     * @param context
     */
    fun deleteCollectedLogs(context: Context){
        Log.d(TAG, "Deleting collected debug logs")
        CollectorService.getFile(context).delete()
        Toast.makeText(context, R.string.clearLogsSuccess, Toast.LENGTH_SHORT).show()
    }

    private fun isServiceRunning() = CollectorService.isRunning

}