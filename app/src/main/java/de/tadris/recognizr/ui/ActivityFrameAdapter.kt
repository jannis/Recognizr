package de.tadris.recognizr.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.tadris.recognizr.R
import de.tadris.recognizr.persistence.ActivityFrame
import de.tadris.recognizr.util.DateFormatter
import java.util.concurrent.TimeUnit

/**
 * Recycler-View adapter for [ActivityFrame]s
 *
 * @property data
 */
class ActivityFrameAdapter(
    private var data: List<ActivityFrame>
) : RecyclerView.Adapter<ActivityFrameAdapter.ActivityFrameViewHolder>() {

    class ActivityFrameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val timestampText: TextView = itemView.findViewById(R.id.activityFrameTimestamp)
        val typeText: TextView = itemView.findViewById(R.id.activityFrameType)
        val durationText: TextView = itemView.findViewById(R.id.activityFrameDuration)

    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(data: List<ActivityFrame>){
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ActivityFrameViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_activity_frame, parent, false)
    )

    override fun onBindViewHolder(holder: ActivityFrameViewHolder, position: Int) {
        val context = holder.itemView.context
        val activityFrame = data[position]

        val minutes = TimeUnit.MILLISECONDS.toMinutes(activityFrame.duration).toInt()

        holder.timestampText.text = DateFormatter.formatFor(activityFrame.startTimestamp)
        holder.durationText.text = context.resources.getQuantityString(R.plurals.minuteDuration, minutes, minutes)
        holder.typeText.text = activityFrame.typeId
    }

    override fun getItemCount() = data.size

}