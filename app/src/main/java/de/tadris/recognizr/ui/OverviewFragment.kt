package de.tadris.recognizr.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import de.tadris.recognizr.R
import de.tadris.recognizr.util.TimeFormatter
import kotlin.math.roundToLong

/**
 * Fragment containing general statistics.
 *
 */
class OverviewFragment : Fragment(R.layout.fragment_statistics_overview) {

    companion object {
        private const val TAG = "OverviewFragment"
    }

    private lateinit var chart: PieChart
    private lateinit var activeMinutesText: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!

        chart = root.findViewById(R.id.overviewPieChart)
        activeMinutesText = root.findViewById(R.id.overviewActiveMinutes)
        initChart()

        val viewModel: OverviewViewModel by viewModels()

        viewModel.infoState.observe(viewLifecycleOwner){
            updateInfo(it)
        }
        viewModel.chartState.observe(viewLifecycleOwner){
            Log.d(TAG, "updating chart, durSize=${it.durations.size}")
            updateChart(it)
        }
        viewModel.load(requireContext())

        return root
    }

    private fun initChart(){
        chart.description.text = ""
    }

    private fun updateInfo(state: OverviewInfoState){
        activeMinutesText.text = resources.getQuantityString(R.plurals.overviewActive, state.activeMinutes.toInt(), state.activeMinutes)
    }

    private fun updateChart(state: OverviewChartState){
        val dataSet = transformToDataSet(state)

        dataSet.colors = calculateColors(state)

        dataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float) =
                TimeFormatter.getHoursMinutes(value.roundToLong()) + " h"
        }

        val data = PieData(dataSet)

        chart.data = data
        chart.invalidate()
    }

    private fun calculateColors(state: OverviewChartState) =
        state.durations.entries
            .sortedByDescending { it.value }
            .map { it.key.color }

    private fun transformToDataSet(state: OverviewChartState): PieDataSet {
        val values = state.durations
            .entries
            .sortedByDescending { it.value } // sort by duration, descending
            .map { entry ->
                val hours = entry.value.toFloat() / 1000 / 60 // convert millis -> minutes
                PieEntry(hours, entry.key.id)
            }
        return PieDataSet(values, getString(R.string.chartActivityDurations))
    }

}