package de.tadris.recognizr.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import de.tadris.recognizr.persistence.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * ViewModel for [HistoryFragment]
 *
 */
class HistoryViewModel : ViewModel() {

    private val _historyState = MutableStateFlow(HistoryState(emptyList()))
    val historyState: StateFlow<HistoryState> = _historyState

    /**
     * Loads new history data async
     *
     * @param context
     */
    fun refreshState(context: Context){
        viewModelScope.launch {
            _historyState.update { HistoryState(provideData(context)) }
        }
    }

    private suspend fun provideData(context: Context) = withContext(Dispatchers.IO){
        DataRepository.getInstance(context).db.frontendFrameDao().findAll()
    }

}