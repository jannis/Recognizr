package de.tadris.recognizr.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import de.tadris.recognizr.util.NotificationChannelHelper
import de.tadris.recognizr.R
import de.tadris.recognizr.util.ShareUtils
import de.tadris.recognizr.service.CollectorService

/**
 * Main screen containing:
 * - service control (start/stop)
 * - link to statistics
 * - debug file management (clear/share)
 *
 */
class MainActivity : AppCompatActivity() {

    private lateinit var serviceStatus: TextView
    private lateinit var serviceButton: Button
    private lateinit var shareButton: Button
    private lateinit var clearButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        serviceStatus = findViewById(R.id.serviceStatus)
        serviceButton = findViewById(R.id.serviceButton)
        shareButton = findViewById(R.id.shareButton)
        clearButton = findViewById(R.id.clearButton)

        val viewModel: MainActivityViewModel by viewModels()

        serviceButton.setOnClickListener { viewModel.toggleService(this) }
        shareButton.setOnClickListener { viewModel.shareCollectedLogs(this) }
        clearButton.setOnClickListener { viewModel.deleteCollectedLogs(this) }

        findViewById<View>(R.id.statisticsButton).setOnClickListener { openStatistics() }

        lifecycleScope.launchWhenResumed {
            viewModel.serviceState.collect { refreshStatus(it) }
        }

        NotificationChannelHelper.createChannels(this)
        NotificationChannelHelper.requestPermission(this)
    }

    private fun refreshStatus(isServiceRunning: Boolean) {
        if(isServiceRunning){
            shareButton.isEnabled = false
            clearButton.isEnabled = false
            serviceButton.setText(R.string.actionStop)
            serviceStatus.setText(R.string.infoServiceRunning)
        }else{
            shareButton.isEnabled = true
            clearButton.isEnabled = true
            serviceButton.setText(R.string.actionStart)
            serviceStatus.setText(R.string.infoServiceStopped)
        }
    }

    private fun openStatistics(){
        val intent = Intent(this, StatisticsActivity::class.java)
        startActivity(intent)
    }

}