package de.tadris.recognizr.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.tadris.recognizr.R
import de.tadris.recognizr.persistence.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Fragment containing the ActivityFrame history in form of a list.
 * @see ActivityFrameAdapter
 *
 */
class HistoryFragment : Fragment(R.layout.fragment_statistics_history) {

    private lateinit var adapter: ActivityFrameAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = ActivityFrameAdapter(emptyList())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!

        val recyclerView: RecyclerView = root.findViewById(R.id.historyRecyclerView)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        val viewModel: HistoryViewModel by viewModels()
        lifecycleScope.launchWhenStarted {
            viewModel.historyState.collect {
                updateList(it)
            }
        }

        viewModel.refreshState(requireContext())

        return root
    }

    private fun updateList(state: HistoryState){
        adapter.updateData(state.history)
    }

}