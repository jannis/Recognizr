package de.tadris.recognizr.ui

/**
 * UI-State for [OverviewFragment]
 *
 * @property activeMinutes Active minutes last day
 */
class OverviewInfoState(val activeMinutes: Long)