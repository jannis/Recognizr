package de.tadris.recognizr.ui

import de.tadris.recognizr.persistence.ActivityFrame

/**
 * UI-State of the [HistoryFragment]
 *
 * @property history current list content
 */
data class HistoryState(val history: List<ActivityFrame>)