package de.tadris.recognizr.service

import android.app.ActivityManager
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class CollectorServiceTest {

    @Test
    fun testGetFile(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val file = CollectorService.getFile(context)
        assertTrue(file.canWrite())
    }

    @Test
    fun testStartStopService(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        CollectorService.startService(context)
        assertTrue(CollectorService.isRunning)

        // Check that service is running according to the activity manager
        val activityManager = context.getSystemService(ActivityManager::class.java)
        assertNotNull(
            activityManager.getRunningServices(Int.MAX_VALUE)
                .find { it.service.className == CollectorService::class.java.name }
        )

        CollectorService.stopService(context)
        assertFalse(CollectorService.isRunning)

        assertNull(
            activityManager.getRunningServices(Int.MAX_VALUE)
                .find { it.service.className == CollectorService::class.java.name }
        )
    }

}