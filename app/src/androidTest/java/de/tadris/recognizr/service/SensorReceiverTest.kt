package de.tadris.recognizr.service

import android.hardware.Sensor
import android.hardware.SensorManager
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import de.tadris.recognizr.recording.SensorDataListener
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

@RunWith(AndroidJUnit4::class)
class SensorReceiverTest {

    @Test
    fun testReceiveSample(){
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val sensorManager = appContext.getSystemService(SensorManager::class.java)
        val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)

        val listener: SensorDataListener = mock()
        val receiver = SensorReceiver(listener)

        // @NonNull Sensor sensor, int accuracy, long timestamp, float[] values
        //receiver.onSensorChanged(SensorEvent(sensor, 0, 0L, floatArrayOf()))
        // TODO
    }

    /**
     * Test that the receiver doesn't crash if null samples are received
     */
    @Test
    fun testReceiveNull(){
        val listener: SensorDataListener = mock()
        val receiver = SensorReceiver(listener)
        receiver.onSensorChanged(null)

        verify(listener, times(0)).receive(any()) // confirm nothing has been emitted
    }

}