package de.tadris.recognizr.service

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import de.tadris.recognizr.recording.SensorDataListener
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.*

@RunWith(AndroidJUnit4::class)
@MediumTest
class SensorReceiverManagerTest {

    @Test
    fun testReceiveSensorEvents() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        val listener: SensorDataListener = mock()
        val receiverManager = SensorReceiverManager(appContext, listener)

        receiverManager.start()
        Thread.sleep(500)
        receiverManager.stop()

        // Verify that sensor events were received
        verify(listener, atLeastOnce()).receive(any())
    }

}