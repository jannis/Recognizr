package de.tadris.recognizr.integration

import android.view.View
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.viewpager2.widget.ViewPager2
import de.tadris.recognizr.R
import de.tadris.recognizr.classifier.ActivityType
import de.tadris.recognizr.persistence.ActivityFrame
import de.tadris.recognizr.persistence.AppDatabase
import de.tadris.recognizr.persistence.DataRepository
import de.tadris.recognizr.persistence.FrontendActivityFrameDao
import de.tadris.recognizr.ui.MainActivity
import de.tadris.recognizr.ui.StatisticsActivity
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.containsString
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.kotlin.any
import org.mockito.kotlin.mock

@RunWith(AndroidJUnit4::class)
@LargeTest
class StatisticsTest {

    companion object {
        private val minute = 1000L * 60 // 1 minute in millis
        private val mockData = listOf(
            ActivityFrame(1, ActivityType.WALKING.id, 1 * minute, 2 * minute),
            ActivityFrame(2, ActivityType.WALKING.id, 2 * minute, 3 * minute),
            ActivityFrame(3, ActivityType.CYCLING.id, 3 * minute, 5 * minute),
            ActivityFrame(4, ActivityType.INACTIVE.id, 5 * minute, 6 * minute),
        )
        private val expectedActiveMinutes = 4
    }

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun injectMockDb(){
        val frontEndDao: FrontendActivityFrameDao = mock {
            on { findAll() }.thenAnswer { mockData }
            on { findAllByTimestampAfter(any()) }.thenAnswer { mockData }
        }
        val mockDb: AppDatabase = mock {
            on { frontendFrameDao() }.thenAnswer { frontEndDao }
        }
        DataRepository.injectMock(mockDb)
    }

    @After
    fun clearMockDb(){
        DataRepository.clearMock()
    }

    @Test
    fun testSwipeNavigation(){
        // Open statistics
        onView(withId(R.id.statisticsButton)).perform(click())

        checkViewPagerIndex(0)
        onView(withId(R.id.statisticsViewPager)).perform(swipeLeft())
        Thread.sleep(1000)
        checkViewPagerIndex(1)
        onView(withId(R.id.statisticsViewPager)).perform(swipeRight())
        Thread.sleep(1000)
        checkViewPagerIndex(0)
        onView(withText(R.string.statisticsHistory)).perform(click())
        checkViewPagerIndex(1)
        onView(withText(R.string.statisticsOverview)).perform(click())
        checkViewPagerIndex(0)
    }

    @Test
    fun testStatistics(){
        // Open statistics
        onView(withId(R.id.statisticsButton)).perform(click())

        // Check active minutes is correct
        onView(withId(R.id.overviewActiveMinutes)).check(matches(
            withText(containsString(expectedActiveMinutes.toString()))
        ))

        // Click on history
        onView(withText(R.string.statisticsHistory)).perform(click())
        checkViewPagerIndex(1)

        // TODO check smth
        /*onData(allOf())
            .inAdapterView(withId(R.id.historyRecyclerView))
            .atPosition(0)
            .onChildView(withId(R.id.activityFrameType))
            .check(matches(withText("walking")))*/
    }

    private fun checkViewPagerIndex(index: Int){
        onView(withId(R.id.statisticsViewPager)).check { view, _ ->
            assertEquals(index, (view as ViewPager2).currentItem)
        }
    }

}