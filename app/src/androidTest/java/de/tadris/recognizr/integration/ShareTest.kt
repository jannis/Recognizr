package de.tadris.recognizr.integration

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import de.tadris.recognizr.service.CollectorService
import de.tadris.recognizr.R
import de.tadris.recognizr.ui.MainActivity
import org.hamcrest.Matchers.allOf
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class ShareTest {

    @get:Rule
    val shareIntentRule = IntentsRule()

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testShareFile(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        
        // Prepare logs file
        CollectorService.getFile(context).writeText("Hello world!")

        // Click share button
        onView(withId(R.id.shareButton)).perform(click())

        // Verify a chooser opened for the send action
        intended(allOf(
            hasAction(Intent.ACTION_CHOOSER),
            hasExtra(Intent.EXTRA_INTENT, hasAction(Intent.ACTION_SEND))
        ))
    }

}