package de.tadris.recognizr.integration

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import de.tadris.recognizr.R
import de.tadris.recognizr.service.CollectorService
import de.tadris.recognizr.ui.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ServiceControlTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testStartStop() {
        // Check service not running
        assertFalse(CollectorService.isRunning)
        onView(withId(R.id.serviceStatus)).check(matches(withText(R.string.infoServiceStopped)))
        onView(withId(R.id.serviceButton)).check(matches(withText(R.string.actionStart)))

        // Perform click
        onView(withId(R.id.serviceButton)).perform(click())
        assertTrue(CollectorService.isRunning) // Check service is running

        // Check that view updated
        onView(withId(R.id.serviceStatus)).check(matches(withText(R.string.infoServiceRunning)))
        onView(withId(R.id.serviceButton)).check(matches(withText(R.string.actionStop)))

        // Stop service
        onView(withId(R.id.serviceButton)).perform(click())

        // Check service stopped and UI updated
        assertFalse(CollectorService.isRunning)
        onView(withId(R.id.serviceStatus)).check(matches(withText(R.string.infoServiceStopped)))
        onView(withId(R.id.serviceButton)).check(matches(withText(R.string.actionStart)))
    }


}