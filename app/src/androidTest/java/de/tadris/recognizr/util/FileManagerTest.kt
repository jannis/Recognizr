package de.tadris.recognizr.util

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@SmallTest
class FileManagerTest {

    private lateinit var context: Context

    @Before
    fun setup(){
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @Test
    fun testCreateSharableFileNormal(){
        FileManager.createSharableFile(context, "hello.txt")
    }

    @Test(expected = IllegalArgumentException::class)
    fun testCreateSharableFileEmpty(){
        FileManager.createSharableFile(context, "")
    }

    @Test
    fun testProvide(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val file = FileManager.createSharableFile(context, "sharable.txt")
        val uri = FileManager.provide(context, file)
        println("Result: $uri")
    }

}