package de.tadris.recognizr.util

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class NotificationChannelHelperTest {

    fun testCreateChannels(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        NotificationChannelHelper.createChannels(context)
    }

}