import math

import numpy
from pandas import read_csv
from pandas.plotting import scatter_matrix
from matplotlib import pyplot
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.datasets import load_diabetes
from sklearn2pmml import PMMLPipeline, sklearn2pmml
from sklearn.tree import DecisionTreeRegressor
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType

if __name__ == '__main__':
    rate = 50.0
    samples_per_set = 1024
    frequencies = int(samples_per_set / 2)
    included_frequencies = 128 + 1

    file = "frequencies.csv"
    names = [f"{(i * rate / samples_per_set):.2f} Hz" for i in range(1, frequencies + 1)]
    names.insert(0, "class")
    dataset = read_csv(file, names=names, delimiter=",")

    print(dataset.shape)
    print(dataset.head(20))

    print(dataset.describe())
    print(dataset.groupby('class').size())

    X = dataset.values[:, 1:included_frequencies]
    y = dataset["class"]
    X_train, X_validation, Y_train, Y_validation = train_test_split(X, y, test_size=0.20, random_state=8)

    # models = [
    #     ('LR', LogisticRegression(solver='liblinear', multi_class='ovr')),
    #     ('LDA', LinearDiscriminantAnalysis()),
    #     ('KNN', KNeighborsClassifier()),
    #     ('CART', DecisionTreeClassifier()),
    #     ('NB', GaussianNB()),
    #     ('SVM', SVC(gamma='auto'))
    # ]

    # MLPClassifier(alpha=1e-05, hidden_layer_sizes=(5, 2), random_state=1, solver='lbfgs')

    activityTypes = []
    for t in dataset.groupby("class")["class"]:
        key = t[1].values[0]
        activityTypes.append(key)
#
    # columns = 2
    # fig, axes = plt.subplots(int((len(activityTypes) + 1) / columns), columns)
    # fig.set_figwidth(15)
#
    # for activityClass, ax in zip(activityTypes, axes.ravel()):
    #     entries = []
    #     for entry in dataset.values:
    #         if entry[0] == activityClass:
    #             entries.append(numpy.fromiter((math.sqrt(float(x)) for x in entry[1:included_frequencies]), dtype=float))
    #     print(f"Found {len(entries)} for {activityClass}")
#
    #     ax.matshow(numpy.array(entries), cmap=plt.cm.gray, vmin=0.0, vmax=20.0)
    #     ax.set_title(activityClass)
    #     ax.set_xticks(())
    #     ax.set_yticks(())

    # plt.show()
    # exit(0)

    # model = MLPClassifier(hidden_layer_sizes=(50, 50, 25), random_state=2, solver='adam', max_iter=100, verbose=True, tol=-100)
    # model.fit(X_train, Y_train)
    # predictions = model.predict(X_validation)
    # print(accuracy_score(Y_validation, predictions))
    # print(confusion_matrix(Y_validation, predictions))
    # print(classification_report(Y_validation, predictions))

    # df_cm = pd.DataFrame(
    #     confusion_matrix(Y_validation, predictions),
    #     index=activityTypes,
    #     columns=activityTypes
    # )

    # sn.heatmap(df_cm, annot=True, annot_kws={"size": 16})
    # plt.show()


    # plt.matshow(confusion_matrix(Y_validation, predictions))
    # plt.show()

    classifier = MLPClassifier(hidden_layer_sizes=(50, 50, 25), random_state=2, solver='adam', max_iter=10000, verbose=True, tol=-100)
    pipeline = PMMLPipeline([('classifier', classifier)])
    pipeline.fit(X, y)
    sklearn2pmml(pipeline, 'model.pmml', with_repr=True)
